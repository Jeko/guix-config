;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (srfi srfi-1)
             (guix channels)
             (guix inferior)
             (gnu)
             (nongnu packages linux)
             (nongnu system linux-initrd))

(use-service-modules desktop networking ssh xorg)

(operating-system
 (kernel linux
	 (let*
	     ((channels
	       (list (channel
	              (name 'nonguix)
	              (url "https://gitlab.com/nonguix/nonguix")
	              (commit "cb034164b7163771ac0c906a723c38fd442fc0c3"))
	             (channel
	              (name 'guix)
	              (url "https://git.savannah.gnu.org/git/guix.git")
	              (commit "a4f7d66ce5670a3f51c42d0b152b4b70a2de0073"))))
	      (inferior
	       (inferior-for-channels channels)))
	   (first (lookup-inferior-packages inferior "linux" "5.16.8")))
	 )
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 (locale "fr_FR.utf8")
 (timezone "Europe/Paris")
 (keyboard-layout (keyboard-layout "fr" "bepo" #:options '("ctrl:swapcaps")))
 (host-name "happiness")
 (users (cons* (user-account
                (name "jeko")
                (comment "Jeko")
                (group "users")
                (home-directory "/home/jeko")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video" "lp" "kvm")))
               %base-user-accounts))
 (packages
  (append
   (list (specification->package "nss-certs"))
   %base-packages))
 (services
  (append
   (list (service gnome-desktop-service-type)
         (set-xorg-configuration
          (xorg-configuration
           (keyboard-layout keyboard-layout)))
	 (service openssh-service-type
		  (openssh-configuration
		   (x11-forwarding? #t)
		   (permit-root-login 'without-password)
		   (port-number 10022)
		   (authorized-keys
		    `(("elite" ,(local-file "elite.pub")))))))
   (modify-services %desktop-services
		    (guix-service-type config => (guix-configuration
						  (inherit config)
						  (substitute-urls
						   (append (list "https://substitutes.nonguix.org")
							   %default-substitute-urls))
						  (authorized-keys
						   (append (list (local-file "./signing-key.pub"))
							   %default-authorized-guix-keys)))))))

 (bootloader
  (bootloader-configuration
   (bootloader grub-bootloader)
   (target "/dev/sda")
   (keyboard-layout keyboard-layout)))
 (swap-devices
  (list (uuid "47bb46fe-1245-40a4-903a-cf33f97bea61")))
 (file-systems
  (cons* (file-system
          (mount-point "/")
          (device
           (uuid "12710d8c-6869-4a3a-abff-c9b96e155170"
                 'ext4))
          (type "ext4"))
         %base-file-systems)))
